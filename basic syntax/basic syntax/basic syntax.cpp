// basic syntax.cpp : Defines the entry point for the console application.
//
/*
Create a short program that requests two INTEGERS from the user. The program then decides 
if the first integer input is a multiple of the second integer input and outputs the appropriate response.
If the first integer IS a multiple of the second integer, the other factor is also supplied. For instance, 
if the first integer input is 20 and the second integer input is 5, the answer is "yes and the other factor is 4".
The program should repeat the input and output until the user wishes to quit.

*/

#include <iostream>
#include <iomanip>

using namespace std; 


int main(int argc, char ** argv)
{	
	
	int x, y;
	char check ='y';

	while (check == 'y') {
		cout << "Give me 2 numbers" <<endl;
		cin >> x;
		cin >> y;
		if (x%y == 0) {
			int z = x / y;
			cout << " yes it is divisable and the other number is " << z << endl;

		}
		else {
			cout << "not divisable" << endl;
		}
		cout << "Would You like to continue? (y/n)"<< endl;
		cin >> check;
	}
	return 0;
}

