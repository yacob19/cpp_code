// maximum.cpp : Defines the entry point for the console application.
//

/*
 * Jacob wydick
 * Homework 1
 * 4/1/2017
 * 
 * maximum of int array function
 */

#include <iostream>



using namespace std;
//function finds the maximum integer in an array given the arrays size
int maximum(int* a, int size) {

	int max = a[0]; // initialize the max 
	

	for (int i = 0; i < size; i++) {
		if (a[i] > max) { // iterate over the array and set the max to a[i] if it is bigger then previous max
			max = a[i];
		}
	}
	
	return(max);
}




int main()
{
	int a[] = {1,100,5,5,9 , -100 , 2000};
	int size = 7;
	cout << maximum(a,size) << endl;

	return 0;
}

