// seperation.cpp : Defines the entry point for the console application.
//

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;


int main(int argc , char * argv[])
{	
	if (argc != 2)
	{
		cout << " please enter only the file name you wish to check" << endl;
		return 1;
	}

	ifstream input_file(argv[1]);

	if (!input_file.is_open())
	{
		cout << "couldn't open" << endl;
		return 1;
	}

	
	vector<int> list;

	while (!input_file.eof())
	{
		int n;
		if (input_file >> n) {

			cout << "storing " << n << endl;
			list.push_back(n);
		}
		else { cout << "tried reading an invalid number!" << endl; }
		
	}
	
	
	

    return 0;
}

