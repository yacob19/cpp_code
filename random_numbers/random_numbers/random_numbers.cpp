// random_numbers.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <random>
#include <fstream>
#include <string>

using namespace std;

int main(int argc , char ** argv)
{
	if (argc <4 ) {
		cout << "usage requires 3 inputs, number or numbers , min and max, and optional output file name" << endl;
		return 1;

	}

	int number_of_items = atoi(argv[1]);
	int minimum = atoi(argv[2]);
	int maximum = atoi(argv[3]);
	bool writing_to_file = false;
	string file_name;
	if (argc ==5)
	{
		writing_to_file = true;
		file_name = string(argv[4]);

	}
	
	


	default_random_engine dre;
	uniform_int_distribution<int> rand(minimum, maximum);
	if (writing_to_file)
	{
		ofstream out_file(file_name);
		for (int i = 0; i < number_of_items; i++)
		{
			out_file << rand(dre) << endl;

		}
		cout << "finished writing to " << file_name << endl;
		
	}
	else
	{


		for (int i = 0; i < number_of_items; i++)
		{
			cout << rand(dre) << endl;
		}
	}
    return 0;
	
}

