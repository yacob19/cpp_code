#pragma once
class myArrayClass
{
public:
	myArrayClass(int length );
	// int * p = new int[n]
	~myArrayClass();

	int length();// : returns the length of the array 
	int read(int idx); //: returns the value of data[idx] 
	void write(int idx, int value); //: stores value at data[idx]
	int search(int n);

private:
	int len;
	int * data;
	
};

