//Reading the length of the array, n, from the keyboard.
//2) Reading the n values from the keyboard and putting them in the array.Assume that the user
//enters a valid set of numbers : every number except one appears twice.
//3) Calling the singular2 function to determine the singular value.
//4) Printing the singular value.



#include <iostream>
#include <string>
#include "my_array.h"
using namespace std;


int singular2(myArrayClass  &A)
{
	bool i_match;
	int len = A.length();
	myArrayClass doubles = myArrayClass{ len };
	
	
	
	for (int i = 0; i < len; i++)
	{
		if (doubles.search(A.read(i)) == -1)
		{


			for (int j = i + 1; j < len; j++)
			{
				if (A.read(j) == A.read(i))
				{
					doubles.write(i, A.read(i));
					i_match = true;
					break;
				}
				else
				{
					i_match = false;
				}
			
			}
				
				

			
			

		}
	if(i_match == false)
	{
		return A.read(i);
	}
	}
	return(A.read(len-1));

}

int main()
{


	int n;
	int value;

	cout << "enter the length of your array." << endl;
	cin >> n;
	myArrayClass a{ n };

	cout << "enter your values 1 by 1" << endl;
	for (int i = 0; i < n; i++)
	{
		cin >> value;
		a.write(i, value);
	}

	int sing = singular2(a);
	cout << "singular value is: "<< sing << endl;

	return 0;
}
