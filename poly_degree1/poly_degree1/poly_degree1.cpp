// poly_degree1.cpp : Defines the entry point for the console application.
//

/*
 *Jacob wydick
 *Homework 1
 *4/1/17
 *
 *
 * A class for manipulating first degree polynomials
 * of the form aX + b 
 *  Default will be x
 */
#include <iostream>
#include "poly_degree1.h"


using namespace std;



void poly_degree1::print() 
{
	if (slope == 0)
	{ // when slope == 0 only the intercept should be shown


		cout << "Y = " << intercept << endl;
	}
	else if (slope == 1) // when slope == 1, only the x and intercept should be shown
	{
		if (intercept > 0)
		{ // if interept is positive add a '+' to output
			cout << "Y = x + " << intercept << endl;
		}
		else if (intercept == 0) // if intercept = 0 do not provide in output
		{
			cout << "Y = x " << endl;
		}
		else
		{
			cout << "Y = x " << intercept << endl; // if x is negative '-' will come with the intercept
		}
	}
	else // otherwise include and x
	{
		if (intercept > 0)
		{ // when intercept is positive add the '+' to the output
			cout << "Y = " << slope << "x + " << intercept << endl;
		}
		else if (intercept == 0) // when intercept = 0 only include the slope 
		{
			cout << "Y = " << slope << "x" << endl;
		}
		else // when intercept is negative dont include the '+'
		{
			cout << "Y = " << slope << "x " << intercept << endl;
		}
	}
}

void poly_degree1::update(int slope_new, int intercept_new)
{
	slope = slope_new;
	intercept = intercept_new;
}

void poly_degree1::update(poly_degree1 new_poly)
{
	slope = new_poly.slope;
	intercept = new_poly.intercept;
}

void poly_degree1::add(poly_degree1 rhs)
{
	slope = rhs.slope + slope;
	intercept = rhs.intercept + intercept;
}



void poly_degree1::subtract(poly_degree1 rhs)
{
	slope = slope - rhs.slope;
	intercept = intercept - rhs.intercept;
}

void poly_degree1::scale(int scale_new)
{
	slope = slope * scale_new;
	intercept = intercept * scale_new;
}

poly_degree1::poly_degree1(int slope_new, int intercept_new)
{
	slope = slope_new;
	intercept = intercept_new;
}

poly_degree1::poly_degree1()
{
	slope = 1;
	intercept = 0;
}

