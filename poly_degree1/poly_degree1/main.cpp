
/*
*Jacob wydick
*Homework 1
*4/1/17
*
*/
#include <iostream>

#include "poly_degree1.h"


using namespace std;



int main()
{
	int x = 10;
	int y = -10;
	int r = 4;

	poly_degree1 f = poly_degree1(x, y);
	poly_degree1 g = poly_degree1();
	f.print(); // print 
	g.print();
	g.add(f); //  
	g.print(); //print 
	f.update(x, r);
	g.update(f);
	g.print();
	f.print(); //print
	g.scale(2);
	f.subtract(g);
	f.print(); // print
	f.scale(r); //scale by 4
	f.print();

	return 0;
}
