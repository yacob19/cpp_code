#ifndef POLY_DEGREE1_H
#define POLY_DEGREE1_H


class poly_degree1 {
public:
	poly_degree1();
		
	/**
	 * \brief accepts two ints
	 * \param slope_new 
	 * \param intercept_new 
	 */
	poly_degree1(int slope_new, int intercept_new);// constructs poly given two integers 
		
	void print();
	void update(int slope_new, int intercept_new);
	void update(poly_degree1 new_poly);
	void add(poly_degree1 rhs);
	

	void subtract(poly_degree1 rhs);
	void scale(int scale_new);

private:
	int slope;
	int intercept;
};

#endif
